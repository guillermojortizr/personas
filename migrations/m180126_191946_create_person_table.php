<?php

use yii\db\Migration;

/**
 * Handles the creation of table `person`.
 */
class m180126_191946_create_person_table extends Migration {

    /**
     * @inheritdoc
     */
    public function up() {
        $this->createTable('person', [
            'id' => $this->primaryKey(),
            'name' => $this->string(120)->notNull(),
            'email' => $this->string(120)->unique()->notNull(),
            'phone' => $this->string(60),
            'role' => $this->string(60)
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down() {
        $this->dropTable('persons');
    }

}
