<?php

namespace app\models;

use Yii;
use borales\extensions\phoneInput\PhoneInputValidator;
use borales\extensions\phoneInput\PhoneInputBehavior;

/**
 * This is the model class for table "person".
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $role
 */
class Person extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'person';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['name', 'email'], 'required'],
            [['name', 'email'], 'string', 'max' => 120],
            [['phone', 'role'], 'string', 'max' => 60],
            [['email'], 'unique'],
            [['email'], 'email'],
            [['phone'], PhoneInputValidator::className()]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'email' => Yii::t('app', 'Email'),
            'phone' => Yii::t('app', 'Phone'),
            'role' => Yii::t('app', 'Role'),
        ];
    }

    public function behaviors() {
        $behaviors = ['phoneInput' => PhoneInputBehavior::className()];
        return $behaviors;
    }

    /**
     * @inheritdoc
     * @return PersonQuery the active query used by this AR class.
     */
    public static function find() {
        return new PersonQuery(get_called_class());
    }

}
